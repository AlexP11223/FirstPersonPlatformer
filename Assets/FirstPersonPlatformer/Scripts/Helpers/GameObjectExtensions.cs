﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FirstPersonPlatformer.Helpers
{
    public static class GameObjectExtensions
    {
        public static T FindComponentInChildWithTag<T>(this GameObject parent, string tag) where T : Component
        {
            Transform t = parent.transform;
            foreach (Transform tr in t)
            {
                if (tr.tag == tag)
                {
                    return tr.GetComponent<T>();
                }

                var childResult = tr.gameObject.FindComponentInChildWithTag<T>(tag);
                if (childResult != null)
                {
                    return childResult;
                }

            }
            return null;
        }

        public static T[] FindComponenstInChildrenWithTag<T>(this Transform parent, string tag) where T : Component
        {
            var items = new List<T>();
            FindComponenstInChildrenWithTagInternal(parent, tag, items);
            return items.ToArray();
        }

        private static void FindComponenstInChildrenWithTagInternal<T>(this Transform parent, string tag, List<T> items) where T : Component
        {
            Transform t = parent.transform;
            foreach (Transform tr in t)
            {
                if (tr.tag == tag)
                {
                    items.Add(tr.GetComponent<T>());
                }

                FindComponenstInChildrenWithTagInternal<T>(tr, tag, items);
            }
        }

        public static T FindChildRecursive<T>(this GameObject parent, string name) where T : Component
        {
            Transform t = parent.transform;
            foreach (Transform tr in t)
            {
                if (tr.name == name)
                {
                    return tr.GetComponent<T>();
                }

                var childResult = tr.gameObject.FindChildRecursive<T>(name);
                if (childResult != null)
                {
                    return childResult;
                }

            }
            return null;
        }

        public static void ExecuteAfterDelay(this MonoBehaviour obj, float delaySec, Action action)
        {
            obj.StartCoroutine(ExecuteAfterDelay(delaySec, action));
        }

        private static IEnumerator ExecuteAfterDelay(float delaySec, Action action)
        {
            yield return new WaitForSeconds(delaySec);

            action();
        }
    }
}
