﻿namespace FirstPersonPlatformer.Helpers
{
    public class CancellableEventArgs
    {
        public bool Cancelled = false;
    }
}