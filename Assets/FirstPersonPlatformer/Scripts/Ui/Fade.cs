﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Fade : MonoBehaviour
{
    public delegate void FadeOutCompletedDelegate();
    public event FadeOutCompletedDelegate FadeOutCompleted;

    private Animator _animator;
    
	void Start()
	{
	    _animator = GetComponent<Animator>();
	}

    public void FadeOut()
    {
        _animator.SetTrigger("FadeOut");
    }

    protected virtual void OnFadeOutCompleted()
    {
        FadeOutCompleted?.Invoke();
    }
}
