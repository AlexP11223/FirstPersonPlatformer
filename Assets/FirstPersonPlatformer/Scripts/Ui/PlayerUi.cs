﻿using System.Collections.Generic;
using System.Linq;
using FirstPersonPlatformer.BulletTime;
using UnityEngine;
using UnityEngine.UI;
using FirstPersonPlatformer.Helpers;
using FirstPersonPlatformer.Weapons;

namespace FirstPersonPlatformer.Ui
{
    public class PlayerUi : MonoBehaviour
    {
        private Text _ammoText;
        private Image _bulletTimeChargeImg;
        private Image _healthImg;
        private List<Image> _bloodSplashes = new List<Image>();

        void Start()
        {
            _ammoText = gameObject.FindChildRecursive<Text>("AmmoValue");
            _healthImg = gameObject.FindChildRecursive<Image>("HealthValue");
            _bulletTimeChargeImg = gameObject.FindChildRecursive<Image>("BulletTimeChargeValue");
            foreach (Transform child in gameObject.FindChildRecursive<Transform>("Blood"))
            {
                _bloodSplashes.Add(child.GetComponent<Image>());
                child.gameObject.SetActive(false);
            }
        }
    
        public void UpdateGun(Gun gun)
        {
            _ammoText.text = gun.IsReloading ? "--" : gun.AmmoLeftInClip.ToString();
        }
    
        public void UpdateHealth(float health)
        {
            _healthImg.fillAmount = health / 100f;
        }
    
        public void UpdateBulletTime(BulletTimeController bulletTime)
        {
            _bulletTimeChargeImg.fillAmount = bulletTime.Charge;
        }
    
        public void TakeHit(float fadeTime)
        {
            var bloodSplash = _bloodSplashes
                .OrderBy(i => Random.value)
                .FirstOrDefault(i => !i.IsActive());
            if (bloodSplash != null)
            {
                bloodSplash.GetComponent<BloodSplash>().FadeTime = fadeTime;
                bloodSplash.gameObject.SetActive(true);
            }
        }
    }
}
