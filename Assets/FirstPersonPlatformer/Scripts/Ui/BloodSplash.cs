﻿using FirstPersonPlatformer.Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace FirstPersonPlatformer.Ui
{
    public class BloodSplash : MonoBehaviour
    {
        public float FadeTime = 2f;

        private Image _img;
        
        void Awake()
        {
            _img = GetComponent<Image>();
        }

        void OnEnable()
        {
            transform.Rotate(0f, 0f, Random.Range(0f, 360f));

            var color = _img.color;
            color.a = 1f;
            _img.color = color;
            
            _img.CrossFadeAlpha(0f, FadeTime, false);

            this.ExecuteAfterDelay(FadeTime, () =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}
