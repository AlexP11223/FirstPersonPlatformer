﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace FirstPersonPlatformer.Ui.Menu
{
    public class GameOverMenu : MonoBehaviour
    {
        public void OnLoadCheckpoint()
        {
            GameManager.LoadLastSave();
        }

        public void OnExitToMenu()
        {
            SceneManager.LoadScene(Scenes.MainMenu);
        }
    }
}
