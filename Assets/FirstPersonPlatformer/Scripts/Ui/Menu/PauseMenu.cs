﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace FirstPersonPlatformer.Ui.Menu
{
    public class PauseMenu : MonoBehaviour
    {
        public PauseManager PauseManager;

        public void OnResume()
        {
            PauseManager.Resume();
        }

        public void OnLoadCheckpoint()
        {
            PauseManager.Resume();
            GameManager.LoadLastSave();
        }

        public void OnExitToMenu()
        {
            PauseManager.Resume();
            SceneManager.LoadScene(Scenes.MainMenu);
        }

        public void OnExit()
        {
            Application.Quit();
        }
    }
}
