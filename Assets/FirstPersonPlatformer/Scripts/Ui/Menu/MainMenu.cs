﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace FirstPersonPlatformer.Ui.Menu
{
    public class MainMenu : MonoBehaviour
    {
        public void Awake()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        public void OnNewGame()
        {
            SceneManager.LoadScene(Scenes.Level1);
        }

        public void OnExit()
        {
            Application.Quit();
        }
    }
}
