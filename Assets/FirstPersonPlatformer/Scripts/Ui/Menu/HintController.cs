﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace FirstPersonPlatformer.Ui.Menu
{
    public class HintController : MonoBehaviour
    {
        public string[] Hints;

        public bool DisableHints = false;

        public Text HintTextBox;

        public void UpdateHint()
        {
            bool showHint = Hints.Any() && !DisableHints;

            HintTextBox.gameObject.SetActive(showHint);

            if (showHint)
            {
                string hint = Hints[Random.Range(0, Hints.Length)];
                HintTextBox.text = $"Hint: {hint}";
            }
        }
	
        void OnEnable()
        {
            UpdateHint();
        }
    }
}
