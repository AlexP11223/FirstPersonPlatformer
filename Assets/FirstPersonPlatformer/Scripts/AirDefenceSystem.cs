﻿using UnityEngine;

namespace FirstPersonPlatformer
{
    public class AirDefenceSystem : MonoBehaviour
    {
        public GameObject MissileSpawn;
        public GameObject MissilePrefab;

        public void Shoot(Transform target)
        {
            var missile = Instantiate(MissilePrefab, MissileSpawn.transform);
            missile.transform.localPosition = Vector3.zero;
            missile.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
            missile.transform.parent = null;

            missile.GetComponent<FlyingObject>().Move(target);
        }
    }
}
