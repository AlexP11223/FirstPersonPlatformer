﻿using System.Collections.Generic;
using System.Linq;
using FirstPersonPlatformer.Weapons;
using UnityEngine;

namespace FirstPersonPlatformer
{
    [RequireComponent(typeof(Animator))]
    public class SoldierArsenal : MonoBehaviour
    {

        public Transform RightGunBone;
        public Transform LeftGunBone;
        public Arsenal[] Arsenals;

        private Dictionary<string, Arsenal> _arsenals;

        private Animator _animator;

        void Awake()
        {
            _arsenals = Arsenals.ToDictionary(a => a.Name);

            _animator = GetComponent<Animator>();
            if (_arsenals.Any())
            {
                SetArsenal(_arsenals.First().Key);
            }
        }

        public void SetArsenal(string name)
        {
            SetArsenal(_arsenals[name]);
        }

        private void SetArsenal(Arsenal arsenal)
        {
            if (RightGunBone.childCount > 0)
                Destroy(RightGunBone.GetChild(0).gameObject);
            if (LeftGunBone.childCount > 0)
                Destroy(LeftGunBone.GetChild(0).gameObject);

            if (arsenal.RightGun != null)
            {
                GameObject newRightGun = Instantiate(arsenal.RightGun);
                newRightGun.transform.parent = RightGunBone;
                newRightGun.transform.localPosition = Vector3.zero;
                newRightGun.transform.localRotation = Quaternion.Euler(90, 0, 0);
                newRightGun.GetComponent<Gun>().Owner = gameObject;
            }
            if (arsenal.LeftGun != null)
            {
                GameObject newLeftGun = Instantiate(arsenal.LeftGun);
                newLeftGun.transform.parent = LeftGunBone;
                newLeftGun.transform.localPosition = Vector3.zero;
                newLeftGun.transform.localRotation = Quaternion.Euler(90, 0, 0);
                newLeftGun.GetComponent<Gun>().Owner = gameObject;
            }

            _animator.runtimeAnimatorController = arsenal.AnimatorController;
        }

        [System.Serializable]
        public struct Arsenal
        {
            public string Name;
            public GameObject RightGun;
            public GameObject LeftGun;
            public RuntimeAnimatorController AnimatorController;
        }
    }
}
