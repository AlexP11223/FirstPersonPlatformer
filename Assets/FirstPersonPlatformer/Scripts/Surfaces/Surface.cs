﻿using UnityEngine;

namespace FirstPersonPlatformer.Surfaces
{
    public class Surface : MonoBehaviour
    {
        public SurfaceType Type;

        public static SurfaceType DetectType(Transform transform)
        {
            var surface = transform.GetComponent<Surface>();
            if (surface != null)
            {
                return surface.Type;
            }

            return SurfaceType.Unknown;
        }

        public static SurfaceType DetectType(GameObject obj)
        {
            return DetectType(obj.transform);
        }
    }
}
