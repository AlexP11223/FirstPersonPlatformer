﻿namespace FirstPersonPlatformer.Surfaces
{
    public enum SurfaceType
    {
        Unknown,
        Dirt,
        Concrete,
        Asphalt,
        Wood,
        Metal,
        Glass,
        Humanoid
    }
}
