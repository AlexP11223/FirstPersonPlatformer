﻿using UnityEngine;

namespace FirstPersonPlatformer
{
    public class Unparent : MonoBehaviour
    {
        void Start()
        {
            gameObject.transform.parent = null;
        }
    }
}