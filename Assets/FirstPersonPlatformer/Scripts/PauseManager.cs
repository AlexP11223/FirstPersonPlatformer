﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace FirstPersonPlatformer
{
    public class PauseManager : MonoBehaviour
    {
        public GameObject PauseScreen;
        public GameObject CharacterController;

        private CursorLockMode _prevLockMode = CursorLockMode.Locked;
        private bool _prevCursorVisibility = false;
        private float _prevTimeScale = 1.0f;
        private float _prevFixedDelta = 0.02f;

        private bool _isPaused = false;
        public bool IsPaused
        {
            get { return _isPaused; }
            private set
            {
                if (_isPaused == value)
                {
                    return;
                }

                if (!_isPaused) // save current state when going to pause
                {
                    _prevLockMode = Cursor.lockState;
                    _prevCursorVisibility = Cursor.visible;
                    _prevTimeScale = Time.timeScale;
                    _prevFixedDelta = Time.fixedDeltaTime;
                }

                _isPaused = value;

                PauseScreen.SetActive(IsPaused);
                CharacterController.GetComponent<FirstPersonController>().enabled = !IsPaused;
                AudioListener.pause = IsPaused;
                Time.timeScale = IsPaused ? 0.0f : _prevTimeScale;
                Time.fixedDeltaTime = IsPaused ? 0.0f : _prevFixedDelta;
                Cursor.lockState = IsPaused ? CursorLockMode.None : _prevLockMode;
                Cursor.visible = IsPaused ? true : _prevCursorVisibility;
            }
        }

        void Update ()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                TogglePause();
            }
        }

        public void TogglePause()
        {
            IsPaused = !IsPaused;
        }

        public void Resume()
        {
            IsPaused = false;
        }
    }
}
