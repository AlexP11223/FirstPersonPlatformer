﻿using System.IO;
using FirstPersonPlatformer.BulletTime;
using FirstPersonPlatformer.Helpers;
using FirstPersonPlatformer.Serialization;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace FirstPersonPlatformer
{
    public class GameManager : MonoBehaviour
    {
        /// <summary>
        /// Kind of singleton, but not shared between scenes
        /// </summary>
        public static GameManager Instance { get; private set; }

        public SaveHandler SaveHandler;

        private static string SaveFilePath => Application.persistentDataPath + "/save.dat";

        private static SaveData _pendingLoadingSaveData;

        private GameObject _gameOverScreen;

        void Start()
        {
            Instance = this;

            if (_pendingLoadingSaveData != null)
            {
                Debug.Log("Continuing loading save");
                FinishSaveLoading(); // execution order is set to +1000
            }

            _gameOverScreen = GameObject.Find("GameOver").transform.Find("GameOverScreen").gameObject;
        }

        public void GameOver()
        {
            _gameOverScreen.SetActive(true);

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            TimeManager.StopSlowMotion();
        }

        public void Save()
        {
            File.WriteAllText(SaveFilePath, SaveHandler.Save().ToJson());

            Debug.Log($"Saved game to {SaveFilePath}");
        }

        public static bool HasSave()
        {
            return File.Exists(SaveFilePath);
        }

        public static void LoadLastSave()
        {
            Assert.IsTrue(HasSave());

            _pendingLoadingSaveData = SaveData.FromJson(File.ReadAllText(SaveFilePath));

            Debug.Log($"Started loading {_pendingLoadingSaveData.CurrentScene} from {SaveFilePath}");

            SceneManager.LoadScene(_pendingLoadingSaveData.CurrentScene);
        }

        private void FinishSaveLoading()
        {
            SaveHandler.Load(_pendingLoadingSaveData);

            _pendingLoadingSaveData = null;

            Debug.Log("Finished loading save");
        }
    }
}
