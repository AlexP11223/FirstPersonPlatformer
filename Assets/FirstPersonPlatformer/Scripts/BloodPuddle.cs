﻿using System.Linq;
using UnityEngine;

namespace FirstPersonPlatformer
{
    public class BloodPuddle : MonoBehaviour
    {
        public float Speed = 0.01f;
        public float MinEndSize = 0.04f;
        public float MaxEndSize = 0.055f;

        private float _endSize;

        void OnEnable()
        {
            var parts = GetComponentsInChildren<Transform>().ToList();
            parts.Add(transform);

            foreach (var part in parts)
            {
                part.transform.Rotate(0f, Random.Range(0f, 360f), 0f);
            }

            _endSize = Random.Range(MinEndSize, MaxEndSize);
        }
	
        void Update()
        {
            if (transform.localScale.x > _endSize)
            {
                enabled = false;
                return;
            }

            transform.localScale += new Vector3(Speed, Speed, Speed) * Time.deltaTime;
        }
    }
}
