﻿using UnityEngine;

namespace FirstPersonPlatformer
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(DestructibleObject))]
    public class Alarm : MonoBehaviour
    {
        public AudioSource Sound;

        public bool IsTriggered => _animator.GetBool("IsTriggered");

        public bool IsDestroyed => _destructibleObject.IsDead;

        private Animator _animator;
        private DestructibleObject _destructibleObject;
        
        void Start()
        {
            _animator = GetComponent<Animator>();
            _destructibleObject = GetComponent<DestructibleObject>();

        }

        public void Trigger()
        {
            _animator.SetBool("IsTriggered", true);
            Sound.Play();
        }

        public void Stop()
        {
            _animator.SetBool("IsTriggered", false);
            Sound.Stop();
        }
    }
}
