﻿using UnityEngine;

namespace FirstPersonPlatformer
{
    [RequireComponent(typeof(Animator))]
    public class Door : MonoBehaviour
    {
        public string OpenedAnimationFlagName = "Opened";

        public AudioSource ClosedSound;

        public bool IsOpened => _animator.GetBool(OpenedAnimationFlagName);

        private Animator _animator;

        void Start()
        {
            _animator = GetComponent<Animator>(); 
        }

        public void Open()
        {
            _animator.SetBool(OpenedAnimationFlagName, true);
        }

        public void Close()
        {
            _animator.SetBool(OpenedAnimationFlagName, false);

            ClosedSound.Play();
        }

        public void Toggle()
        {
            if (IsOpened)
            {
                Close();
            }
            else
            {
                Open();
            }
        }
    }
}