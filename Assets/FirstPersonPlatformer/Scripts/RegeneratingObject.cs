﻿using System;
using UnityEngine;

namespace FirstPersonPlatformer
{
    [RequireComponent(typeof(DestructibleObject))]
    public class RegeneratingObject : MonoBehaviour
    {
        public float DelayAfterDamage = 2f;
        public float Rate = 4f;

        private float _maxHealth;

        private DestructibleObject _destructibleObject;
        private float _lastDamageTime;

        void Start()
        {
            _destructibleObject = GetComponent<DestructibleObject>();

            _maxHealth = _destructibleObject.Health;

            _destructibleObject.Damaged += OnObjectDamaged;
        }

        void OnDestroy()
        {
            _destructibleObject.Damaged -= OnObjectDamaged;
        }

        private void OnObjectDamaged(GameObject sender, float damage, float healthBefore)
        {
            _lastDamageTime = Time.time;
        }

        void Update()
        {
            if (_destructibleObject.IsDead)
            {
                enabled = false;
                return;
            }

            if (Time.time - _lastDamageTime >= DelayAfterDamage)
            {
                _destructibleObject.Health = Mathf.Min(_maxHealth, _destructibleObject.Health + Rate * Time.deltaTime);
            }
        }
    }
}
