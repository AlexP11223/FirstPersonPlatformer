﻿using System;
using System.Linq;
using FirstPersonPlatformer.Ai;
using UnityEngine;

namespace FirstPersonPlatformer.Serialization
{
    public class Level1SaveHandler : SaveHandler
    {
        public MilitaryBaseManager MilitaryBaseManager;

        public Collider[] Triggers;

        public Transform SoldiersGroup;

        public GameObject Helicopter;

        public override SaveData Save()
        {
            var saveData = base.Save();

            saveData.Level1 = new Level1Data
            {
                Alarms = MilitaryBaseManager.Alarms.Select(a => new AlarmData
                {
                    IsDestroyed = a.IsDestroyed,
                    IsTriggered = a.IsTriggered
                }).ToArray(),
                Soldiers = Soldiers.FindAll(SoldiersGroup).Select(s => new SoldierData
                {
                    Position = s.transform.position,
                    Rotation = s.transform.rotation,
                    State = s.State
                }).ToArray(),
                Triggers = Triggers.Select(t => new TriggerData
                {
                    IsEnabled = t.enabled
                }).ToArray(),
            };

            return saveData;
        }

        public override void Load(SaveData saveData)
        {
            base.Load(saveData);

            var levelSaveData = saveData.Level1;
            
            foreach (var alarmSavedAlarmPair in MilitaryBaseManager.Alarms.Zip(levelSaveData.Alarms, Tuple.Create))
            {
                if (alarmSavedAlarmPair.Item2.IsTriggered)
                {
                    alarmSavedAlarmPair.Item1.Trigger();
                }
                if (alarmSavedAlarmPair.Item2.IsDestroyed)
                {
                    alarmSavedAlarmPair.Item1.GetComponent<IDamageable>().TakeDamage(9999);
                }
            }
            
            foreach (var soldierSavedSoldierPair in Soldiers.FindAll(SoldiersGroup).Zip(levelSaveData.Soldiers, Tuple.Create))
            {
                soldierSavedSoldierPair.Item1.transform.position = soldierSavedSoldierPair.Item2.Position;
                soldierSavedSoldierPair.Item1.transform.rotation = soldierSavedSoldierPair.Item2.Rotation;
                if (soldierSavedSoldierPair.Item2.State == SoldierState.Dead)
                {
                    soldierSavedSoldierPair.Item1.GetComponent<IDamageable>().TakeDamage(999);
                }
            }
            
            foreach (var triggerSavedTriggerPair in Triggers.Zip(levelSaveData.Triggers, Tuple.Create))
            {
                triggerSavedTriggerPair.Item1.enabled = triggerSavedTriggerPair.Item2.IsEnabled;
            }
        }
    }
}