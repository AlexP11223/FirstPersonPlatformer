﻿using System;
using FirstPersonPlatformer.Ai;
using Newtonsoft.Json;
using UnityEngine;

namespace FirstPersonPlatformer.Serialization
{
    [Serializable]
    public class SaveData
    {
        public string CurrentScene { get; set; }

        public PlayerData Player { get; set; }

        public Level1Data Level1 { get; set; }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static SaveData FromJson(string json)
        {
            return JsonConvert.DeserializeObject<SaveData>(json);
        }
    }

    [Serializable]
    public class PlayerData
    {
        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }

        public bool HasParachute { get; set; } // quick fix to avoid triggering parachute on start point when loading next saves

        public GunData AssaultRifle { get; set; }
    }

    [Serializable]
    public class GunData
    {
        public int AmmoLeftInClip { get; set; }
    }

    [Serializable]
    public class Level1Data
    {
        public AlarmData[] Alarms { get; set; }

        public TriggerData[] Triggers { get; set; }

        public SoldierData[] Soldiers { get; set; }
    }
    
    [Serializable]
    public class AlarmData
    {
        public bool IsTriggered { get; set; }
        public bool IsDestroyed { get; set; }
    }
    
    [Serializable]
    public class SoldierData
    {
        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }

        public SoldierState State { get; set; }
    }
    
    [Serializable]
    public class TriggerData
    {
        public bool IsEnabled { get; set; }
    }
}