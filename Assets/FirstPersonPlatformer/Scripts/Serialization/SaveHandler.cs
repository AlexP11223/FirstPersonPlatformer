﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace FirstPersonPlatformer.Serialization
{
    public abstract class SaveHandler : MonoBehaviour
    {
        private SaveData _currentSave;

        public Player Player;

        public virtual SaveData Save()
        {
            var saveData = _currentSave ?? new SaveData();

            saveData.CurrentScene = SceneManager.GetActiveScene().name;

            saveData.Player = new PlayerData
            {
                Position = Player.transform.position,
                Rotation = Player.transform.rotation,
                HasParachute = Player.GetComponent<Parachute>().enabled,
                AssaultRifle = new GunData
                {
                    AmmoLeftInClip = Player.CurrentGun.AmmoLeftInClip // TODO: guns
                }
            };

            _currentSave = saveData;

            return saveData;
        }

        public virtual void Load(SaveData saveData)
        {
            Player.transform.position = saveData.Player.Position;
            Player.transform.rotation = saveData.Player.Rotation;
            Player.GetComponent<Parachute>().enabled = saveData.Player.HasParachute;
            Player.CurrentGun.AmmoLeftInClip = saveData.Player.AssaultRifle.AmmoLeftInClip;

            _currentSave = saveData;
        }
    }
}
