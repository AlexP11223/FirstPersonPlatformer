﻿using FirstPersonPlatformer.Helpers;
using UnityEngine;

namespace FirstPersonPlatformer
{
    [RequireComponent(typeof(Animator))]
    public class SoldierActions : MonoBehaviour
    {
        private Animator _animator;

        private const int DamageAnimationsCount = 3;
        private int _lastDamageAnimation = -1;

        void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void Stay()
        {
            _animator.SetBool("Aiming", false);
            _animator.SetFloat("Speed", 0f);
        }

        public void Walk()
        {
            _animator.SetBool("Aiming", false);
            _animator.SetFloat("Speed", 0.5f);
        }

        public void Run()
        {
            _animator.SetBool("Aiming", false);
            _animator.SetFloat("Speed", 1f);
        }

        public void Attack()
        {
            Aim();
            _animator.SetTrigger("Attack");
        }

        public void Die()
        {
            _animator.SetTrigger("Death");
            this.ExecuteAfterDelay(2.5f, () =>
            {
                var blood = gameObject.FindComponentInChildWithTag<Transform>(Tags.Blood);
                if (blood != null)
                {
                    blood.gameObject.SetActive(true);
                }

                var rigCcollider = transform.Find("RigAss").GetComponent<CapsuleCollider>();
                rigCcollider.center = new Vector3(0.07f, 0.1f, 0f);
                rigCcollider.radius = 0.15f;
                rigCcollider.height = 0.65f;

                var headCcollider = gameObject.FindComponentInChildWithTag<BoxCollider>(Tags.Head);
                headCcollider.center = new Vector3(-0.03f, 0f, 0f);
                headCcollider.size = new Vector3(0.12f, 0.12f, 0.1f);
            });
        }

        public void Damage()
        {
            if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Death") || !_animator.GetCurrentAnimatorStateInfo(1).IsName("Null"))
            {
                return;
            }
            int id = Random.Range(0, DamageAnimationsCount);
            if (DamageAnimationsCount > 1)
                while (id == _lastDamageAnimation)
                    id = Random.Range(0, DamageAnimationsCount);
            _lastDamageAnimation = id;
            _animator.SetInteger("DamageID", id);
            _animator.SetTrigger("Damage");
        }

        public void Jump()
        {
            _animator.SetBool("Squat", false);
            _animator.SetFloat("Speed", 0f);
            _animator.SetBool("Aiming", false);
            _animator.SetTrigger("Jump");
        }

        public void Aim()
        {
            _animator.SetBool("Squat", false);
            _animator.SetFloat("Speed", 0f);
            _animator.SetBool("Aiming", true);
        }

        public void Sit()
        {
            _animator.SetBool("Squat", !_animator.GetBool("Squat"));
            _animator.SetBool("Aiming", false);
        }

        public void Reload()
        {
            Aim();
            _animator.SetTrigger("Reload");
        }
    }
}
