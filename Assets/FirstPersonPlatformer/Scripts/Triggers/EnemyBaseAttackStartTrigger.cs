﻿using FirstPersonPlatformer.Ai;
using UnityEngine;
using FirstPersonPlatformer.Helpers;

namespace FirstPersonPlatformer.Triggers
{
    public class EnemyBaseAttackStartTrigger : PlayerTrigger
    {
        public MilitaryBaseManager MilitaryBaseManager;

        public Helicopter Helicopter;
        public Transform[] HelicopterTargets;
        public Transform HelicopterWayPoint;

        public AirDefenceSystem[] AirDefenceSystems;
        public Transform[] AirDefenceTargets;

        public DestructibleObject[] DestroyedObjects;

        public GameObject SoldierGroup;
        public GameObject SoldierGroup2;
        public GameObject TowerSoldierGroup;

        public Soldier AirDefGunner;
        public Transform AirDefGunnerPoint;

        public override void Execute(Collider collision)
        {
            Helicopter.Shoot(HelicopterTargets[0], HelicopterGunId.Left);
            this.ExecuteAfterDelay(0.2f, () =>
            {
                Helicopter.Shoot(HelicopterTargets[1], HelicopterGunId.Right);
            });

            var player = collision.gameObject;

            this.ExecuteAfterDelay(2.5f, () =>
            {
                MilitaryBaseManager.TriggerAlarm();

                this.ExecuteAfterDelay(1.5f, () =>
                {
                    foreach (var destroyedObject in DestroyedObjects)
                    {
                        destroyedObject.TakeDamage(9999999);
                    }

                    for (int i = 0; i < AirDefenceTargets.Length; i++)
                    {
                        AirDefenceSystems[i].Shoot(AirDefenceTargets[i]);
                    }

                    Soldiers.SendToAttack(SoldierGroup, player.transform);

                    AirDefGunner.Move(AirDefGunnerPoint);

                    this.ExecuteAfterDelay(0.5f, () =>
                    {
                        Helicopter.Move(HelicopterWayPoint);
                    });

                    this.ExecuteAfterDelay(1f, () =>
                    {
                        Soldiers.AttackStationary(TowerSoldierGroup, player.transform, 170f);
                    });

                    this.ExecuteAfterDelay(7f, () =>
                    {
                        Soldiers.SendToAttack(SoldierGroup2, player.transform);
                    });

                });
            });
        }
    }
}