﻿using FirstPersonPlatformer.Ai;
using FirstPersonPlatformer.Helpers;
using FirstPersonPlatformer.Weapons;
using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class MainBuildingEnterTrigger : PlayerTrigger
    {
        public Door[] Doors;

        public DestructibleObject NextRoomDoor;

        public Transform GasEmittersGroup;
        public DamageZone GasDamageZone;

        public GameObject SoldiersGroup;

        public Soldier AttackingSoldier;
        public Transform AttackingSoldierWayPoint;

        public Soldier ExecutingSoldier;
        public DestructibleObject Scientist;
        public Transform ScientistShootingTarget;

        public GameObject Helicopter;

        public AudioSource WtfSound;

        public override void Execute(Collider collision)
        {
            var player = collision.gameObject;

            foreach (var door in Doors)
            {
                this.ExecuteAfterDelay(0.2f, () =>
                {
                    door.Close();
                });
            }
            
            this.ExecuteAfterDelay(4f, () =>
            {
                var gasEmitters = GasEmittersGroup.GetComponentsInChildren<GasEmitter>();
                foreach (var gasEmitter in gasEmitters)
                {
                    gasEmitter.Enable();
                }

                GasDamageZone.enabled = true;

                NextRoomDoor.Destroyed += o =>
                {
                    this.ExecuteAfterDelay(6f, () =>
                    {
                        foreach (var gasEmitter in gasEmitters)
                        {
                            gasEmitter.Disable();
                        }
                    });
                    this.ExecuteAfterDelay(20f, () => { GasDamageZone.enabled = false; });

                    Soldiers.AttackStationary(SoldiersGroup, player.transform, 100f);

                    this.ExecuteAfterDelay(0.5f, () => { WtfSound.Play(); });

                    this.ExecuteAfterDelay(1f, () =>
                    {
                        ExecutingSoldier.AttackStationary(ScientistShootingTarget);

                        Scientist.Destroyed += s =>
                        {
                            ExecutingSoldier.AttackStationary(player.transform);
                        };
                    });
                };

                if (AttackingSoldier.State != SoldierState.Dead)
                {
                    AttackingSoldier.Move(AttackingSoldierWayPoint);
                }
            });

            if (Helicopter != null) // TODO: handle in save
            {
                Destroy(Helicopter);
            }
        }
    }
}