﻿using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class SaveTrigger : PlayerTrigger
    {
        public override void Execute(Collider collision)
        {
            GameManager.Instance.Save();
        }
    }
}