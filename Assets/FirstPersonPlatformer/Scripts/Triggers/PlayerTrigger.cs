﻿using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public abstract class PlayerTrigger : MonoBehaviour, ITrigger
    {
        public virtual bool CanExecute(Collider collider)
        {
            return collider.CompareTag(Tags.Player);
        }

        public abstract void Execute(Collider collider);
    }
}