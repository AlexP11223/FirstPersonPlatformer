﻿using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class VolumeChangeTrigger : PlayerTrigger
    {
        public AudioVolumeController AudioVolumeController;

        [Range(0f, 1f)]
        public float Volume = 1.0f;

        public override void Execute(Collider collision)
        {
            Debug.Log($"Volume of {AudioVolumeController.name} changed to {Volume}");
            AudioVolumeController.SetVolume(Volume);
        }
    }
}