﻿using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public interface ITrigger
    {
        bool CanExecute(Collider collision);

        void Execute(Collider collision);
    }
}
