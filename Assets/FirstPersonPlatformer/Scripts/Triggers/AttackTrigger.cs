﻿using FirstPersonPlatformer.Ai;
using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class AttackTrigger : PlayerTrigger
    {
        public GameObject EnemiesGroup;

        public override void Execute(Collider collision)
        {
            var player = collision.gameObject;

            var enemies = EnemiesGroup.GetComponentsInChildren<MeleeMonster>();

            foreach (var enemy in enemies)
            {
                enemy.Attack(player.transform);
            }
        }
    }
}