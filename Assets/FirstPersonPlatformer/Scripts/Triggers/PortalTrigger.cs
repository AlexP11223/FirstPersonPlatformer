﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace FirstPersonPlatformer.Triggers
{
    public class PortalTrigger : PlayerTrigger
    {
        public override void Execute(Collider collision)
        {
            SceneManager.LoadScene(Scenes.Level2);
        }
    }
}