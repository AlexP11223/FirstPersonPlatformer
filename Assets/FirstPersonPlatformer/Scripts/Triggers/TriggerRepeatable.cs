﻿using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(ITrigger))]
    public class TriggerRepeatable : MonoBehaviour
    {
        private ITrigger _trigger;

        void Start()
        {
            _trigger = GetComponent<ITrigger>();
        }

        void OnTriggerEnter(Collider collider)
        {
            if (isActiveAndEnabled && _trigger.CanExecute(collider))
            {
                _trigger.Execute(collider);
            }
        }
    }
}