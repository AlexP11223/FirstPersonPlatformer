﻿using System.Linq;
using FirstPersonPlatformer.Ai;
using FirstPersonPlatformer.Helpers;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace FirstPersonPlatformer.Triggers
{
    public class PortalSpawnTrigger : PlayerTrigger
    {
        public GameObject Portal;

        public GameObject LabSoldiersGroup;

        private GameObject _player;

        private Soldier[] _soldiers;

        public override bool CanExecute(Collider collider)
        {
            if (_soldiers == null)
            {
                _soldiers = LabSoldiersGroup.GetComponentsInChildren<Soldier>();
            }
            return base.CanExecute(collider) && _soldiers.All(s => s.State == SoldierState.Dead);
        }

        public override void Execute(Collider collision)
        {
            _player = collision.gameObject;

            this.ExecuteAfterDelay(3f, () =>
            {
                Portal.transform.LookAt(_player.transform);

                Portal.SetActive(true);

                var fp = _player.GetComponent<FirstPersonController>();
                fp.MouseLook.MinimumX = -30;
                fp.MouseLook.MaximumX = 30;
                fp.MouseLook.YSensitivity = 0.2f;
                fp.MouseLook.XSensitivity = 0.2f;
                fp.WalkSpeed = 0.1f;
                fp.RunSpeed = 0.2f;

                var flyingObject = _player.GetComponent<FlyingObject>();
                flyingObject.enabled = true;
                flyingObject.Move(Portal.transform);

                _player.GetComponent<Animator>().enabled = false;

                enabled = true;

                Debug.Log("Portal spawned");
            });
        }

        void Update()
        {
            RotateTo(_player.transform.GetChild(0), Portal.transform);
        }

        private void RotateTo(Transform obj, Transform target)
        {
            Vector3 direction = target.position - obj.position;

            Quaternion rot = Quaternion.LookRotation(direction);

            // slerp to the desired rotation over time
            obj.rotation = Quaternion.Slerp(obj.rotation, rot, 2f * Time.deltaTime);
        }
    }
}