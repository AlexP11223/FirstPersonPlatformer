﻿using FirstPersonPlatformer.Ai;
using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class EnemyBaseEnterTrigger : PlayerTrigger
    {
        public GameObject TowerSoldierGroup;
        public GameObject SoldierGroup;

        public override void Execute(Collider collision)
        {
            var player = collision.gameObject;

            Soldiers.SendToAttack(TowerSoldierGroup, player.transform);

            Soldiers.SendToAttack(SoldierGroup, player.transform);
        }
    }
}