﻿using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(ITrigger))]
    public class TriggerOnce : MonoBehaviour
    {
        private ITrigger _trigger;

        void Start()
        {
            _trigger = GetComponent<ITrigger>();
        }

        void OnTriggerEnter(Collider collider)
        {
            if (isActiveAndEnabled && _trigger.CanExecute(collider))
            {
                enabled = false;
                GetComponent<Collider>().enabled = false;

                _trigger.Execute(collider);
            }
        }
    }
}
