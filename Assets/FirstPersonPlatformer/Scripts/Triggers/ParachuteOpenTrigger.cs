﻿using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class ParachuteOpenTrigger : PlayerTrigger
    {
        public override void Execute(Collider collision)
        {
            var player = collision.gameObject;
            var parachute = player.GetComponent<Parachute>();
            if (parachute.isActiveAndEnabled)
            {
                parachute.Open();
            }
        }
    }
}