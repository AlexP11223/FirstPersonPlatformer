﻿using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class HelicopterWaypointTrigger : MonoBehaviour, ITrigger
    {
        public Transform NextWayPoint;

        public bool CanExecute(Collider collision)
        {
            return collision.GetComponent<Helicopter>() != null;
        }

        public void Execute(Collider collision)
        {
            var helicopter = collision.GetComponent<Helicopter>();

            helicopter.RotateLeft();

            helicopter.Move(NextWayPoint);
        }
    }
}
