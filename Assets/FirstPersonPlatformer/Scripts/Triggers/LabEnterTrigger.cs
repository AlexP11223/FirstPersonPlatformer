﻿using FirstPersonPlatformer.BulletTime;
using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class LabEnterTrigger : PlayerTrigger
    {
        public override void Execute(Collider collision)
        {
            var player = collision.gameObject;

            player.GetComponent<BulletTimeController>().Enable();
        }
    }
}