﻿using FirstPersonPlatformer.Ai;
using UnityEngine;

namespace FirstPersonPlatformer.Triggers
{
    public class NearMainBuildingTrigger : PlayerTrigger
    {
        public GameObject SoldierGroup;

        public override void Execute(Collider collision)
        {
            var player = collision.gameObject;

            Soldiers.AttackStationary(SoldierGroup, player.transform, 90f);
        }
    }
}