﻿using UnityEngine;

namespace FirstPersonPlatformer.Weapons
{
    [RequireComponent(typeof(DestructibleObject))]
    [RequireComponent(typeof(Explosive))]
    public class DestructibleExplosive : MonoBehaviour
    {
        private DestructibleObject _destructibleObject;
        private Explosive _explosive;

        void Start()
        {
            _destructibleObject = GetComponent<DestructibleObject>();
            _explosive = GetComponent<Explosive>();

            _destructibleObject.Destroyed += o =>
            {
                _explosive.Explode();
            };
        }
    }
}
