﻿using System.Linq;
using FirstPersonPlatformer.Helpers;
using UnityEngine;

namespace FirstPersonPlatformer.Weapons
{
    public class Explosive : MonoBehaviour
    {
        public float Damage = 100f;
        public float Radius = 10f;
        public float DamageFadeRate = 10f;
        public float Force = 10f;
        public float DamageSpeed = 200f;
        public float ForceDelay = 10f;

        public void Explode()
        {
            var point = GetComponentInChildren<Collider>().transform;

            var colliders = Physics.OverlapSphere(point.position, Radius);

            foreach (var obj in colliders)
            {
                float distance = Vector3.Distance(point.position, obj.transform.position);
                
                var damageable = obj.GetComponentInParent<IDamageable>();
                if (damageable != null)
                {
                    var damage = Mathf.Clamp(Damage - distance * DamageFadeRate, 0f, Damage);
                    this.ExecuteAfterDelay(distance / DamageSpeed, () =>
                    {
                        damageable.TakeDamage(damage);
                    });
                }
            }

            var rigidbodies = colliders.Select(c => c.GetComponentInParent<Rigidbody>()).Where(rb => rb != null).ToArray();
            this.ExecuteAfterDelay(ForceDelay, () =>
            {
                foreach (var rb in rigidbodies)
                {
                    rb.AddExplosionForce(Force, point.position, Radius, 0.5f, ForceMode.Impulse);
                }
            });
        }
    }
}
