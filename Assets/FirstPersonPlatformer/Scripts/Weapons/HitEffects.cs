﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstPersonPlatformer.Surfaces;
using UnityEngine;

namespace FirstPersonPlatformer.Weapons
{
    public class HitEffects : MonoBehaviour
    {
        [Serializable]
        public struct HitEffect
        {
            public string Name;
            public GameObject Prefab;
        }

        public HitEffect[] HitEffectsPrefabs;

        private Dictionary<string, HitEffect> _hitEffectsDict;

        private static HitEffects _instance;

        void Start()
        {
            _hitEffectsDict = HitEffectsPrefabs.ToDictionary(it => it.Name);

            if (_instance != null)
            {
                Debug.LogWarning($"Detected multiple {nameof(HitEffects)} instances");
            }

            _instance = this;
        }

        public static GameObject GetSurfaceHitEffect(GameObject obj)
        {
            return _instance.SurfaceHitEffect(obj);
        }

        public GameObject SurfaceHitEffect(GameObject obj)
        {
            var surfaceType = Surfaces.Surface.DetectType(obj);
            switch (surfaceType)
            {
                case SurfaceType.Unknown:
                    return _hitEffectsDict["MetalNoHole"].Prefab;
                case SurfaceType.Dirt:
                case SurfaceType.Glass:
                    return _hitEffectsDict["Dirt"].Prefab;
                case SurfaceType.Concrete:
                    return _hitEffectsDict["Concrete"].Prefab;
                case SurfaceType.Asphalt:
                    return _hitEffectsDict["Asphalt"].Prefab;
                case SurfaceType.Wood:
                    return _hitEffectsDict["Wood"].Prefab;
                case SurfaceType.Metal:
                    return _hitEffectsDict["Metal"].Prefab;
                case SurfaceType.Humanoid:
                    return _hitEffectsDict["Humanoid"].Prefab;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
