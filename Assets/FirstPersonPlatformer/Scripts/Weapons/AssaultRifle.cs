﻿using System;
using FirstPersonPlatformer.Helpers;
using UnityEngine;

namespace FirstPersonPlatformer.Weapons
{
    public class AssaultRifle : Gun
    {
        public float FireRate = .10f;
        public float Range = 150;
        public float BulletSpeed = 500;
        public float Spread = 0.01f;
        public int Damage = 20;
        public int ClipSize = 30;
        public float ReloadTime = 2f;

        public string ReloadAnimationTrigger = "Reload";
        
        public ParticleSystem MuzzleFlash;
        public AudioSource ReloadSound;
        public AudioSource ShotSound;

        private Animator _animator;

        //private Transform _gunEnd;

        private float _nextFireTime = 0;

        private new void Start()
        {
            base.Start();
        }

        private new void Awake()
        {
            base.Awake();

            AmmoLeftInClip = ClipSize;

            _animator = GetComponent<Animator>();

            //_gunEnd = transform.gameObject.FindComponentInChildWithTag<Transform>("GunEnd");
        }

        public override void HandleInput()
        {
            if (Input.GetButton("Fire1"))
            {
                Shoot();
            }

            if (Input.GetButtonDown("Reload"))
            {
                Reload();
            }
        }

        public override bool CanShoot()
        {
            return Time.time > _nextFireTime && !IsReloading;
        }

        public override void Shoot(Transform from = null)
        {
            if (from == null)
            {
                from = Owner.transform;
            }

            if (!CanShoot())
            {
                return;
            }

            if (AmmoLeftInClip <= 0)
            {
                Reload();
                return;
            }

            _nextFireTime = Time.time + FireRate;

            AmmoLeftInClip--;

            MuzzleFlash.Play();

            ShotSound.Play();

            var direction = from.forward;
            direction += new Vector3(UnityEngine.Random.Range(-Spread, Spread), UnityEngine.Random.Range(-Spread, Spread), UnityEngine.Random.Range(-Spread, Spread));

            Debug.DrawRay(from.position, direction * Range, Color.yellow, 1f);

            RaycastHit hitInfo;
            if (Physics.Raycast(from.position, direction, out hitInfo, Range, LayerMask))
            {
                var obj = hitInfo.collider.gameObject;

                Debug.Log($"Hit {obj.name} by {name}");

                this.ExecuteAfterDelay(hitInfo.distance / BulletSpeed, () =>
                {
                    var damageable = obj.GetComponentInParent<IDamageable>();
                    if (damageable != null)
                    {
                        var damage = Damage;
                        if (obj.CompareTag(Tags.Head))
                        {
                            Debug.Log("Headshot");
                            damage *= 10;
                        }

                        damageable.TakeDamage(damage);
                    }

                    var hitParticles = HitEffects.GetSurfaceHitEffect(obj);
                    if (hitParticles != null)
                    {
                        var hitInstance = Instantiate(hitParticles, hitInfo.point, Quaternion.LookRotation(hitInfo.normal));
                        hitInstance.transform.parent = hitInfo.collider.transform;
                        Destroy(hitInstance, 120f);
                    }
                });
            }
        }

        public void Reload()
        {
            if (!CanReload())
            {
                return;
            }

            AmmoLeftInClip = ClipSize;

            IsReloading = true;

            if (!String.IsNullOrEmpty(ReloadAnimationTrigger))
            {
                _animator.SetTrigger("Reload");
            }

            ReloadSound.Play();

            this.ExecuteAfterDelay(ReloadTime, () =>
            {
                IsReloading = false;
            });
        }
    }
}
