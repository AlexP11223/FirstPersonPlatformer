﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FirstPersonPlatformer.Weapons
{
    public class DamageZone : MonoBehaviour
    {
        public float Damage = 1.5f;

        public string[] DamageableTags;

        private readonly HashSet<IDamageable> _objectsInside = new HashSet<IDamageable>();

        void OnTriggerEnter(Collider collision)
        {
            var damageable = collision.gameObject.GetComponentInParent<IDamageable>();
            if (damageable != null && (!DamageableTags.Any() || DamageableTags.Contains(collision.tag)))
            {
                Debug.Log($"{collision.gameObject.name} entered {name}");
                _objectsInside.Add(damageable);
            }
        }

        void OnTriggerExit(Collider collision)
        {
            var damageable = collision.gameObject.GetComponentInParent<IDamageable>();
            if (damageable != null && _objectsInside.Contains(damageable))
            {
                Debug.Log($"{collision.gameObject.name} left {name}");
                _objectsInside.Remove(damageable);
            }
        }

        void Update()
        {
            foreach (var obj in _objectsInside)
            {
                obj.TakeDamage(Damage * Time.deltaTime);
            }
        }
    }
}