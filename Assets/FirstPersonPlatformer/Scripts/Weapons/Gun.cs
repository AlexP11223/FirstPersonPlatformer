﻿using FirstPersonPlatformer.Helpers;
using UnityEngine;

namespace FirstPersonPlatformer.Weapons
{
    public abstract class Gun : MonoBehaviour
    {
        public int AmmoLeftInClip { get; set; }

        public delegate void ReloadStartingDelegate(CancellableEventArgs e);
        public event ReloadStartingDelegate ReloadStarting;

        public delegate void ReloadStartedDelegate();
        public event ReloadStartedDelegate ReloadStarted;

        private bool _isReloading = false;
        public bool IsReloading
        {
            get { return _isReloading; }
            protected set
            {
                _isReloading = value;
                OnReloadStarted();
            }
        }

        protected int LayerMask = -5; // -5 - default Ignore Raycast

        [SerializeField]
        private GameObject _owner;


        public GameObject Owner
        {
            get { return _owner; }
            set
            {
                _owner = value;
                
                UpdateLayerMask();
            }
        }

        public abstract void HandleInput();

        public abstract bool CanShoot();

        public abstract void Shoot(Transform from = null);

        public virtual bool CanReload()
        {
            var e = new CancellableEventArgs();
            OnReloadStarting(e);

            return !e.Cancelled;
        }

        protected void Start()
        {
            UpdateLayerMask();
        }

        protected void Awake()
        {
        }

        protected void UpdateLayerMask()
        {
            LayerMask = -5;

            if (Owner != null)
            {
                var ownerCollider = Owner.GetComponentInParent<Collider>();
                if (ownerCollider == null)
                {
                    ownerCollider = GetComponentInParent<Collider>();
                }

                if (ownerCollider != null)
                {
                    LayerMask &= ~(1 << ownerCollider.gameObject.layer);
                }
                else
                {
                    Debug.LogWarning("Gun owner collider not found");
                }
            }
        }

        protected virtual void OnReloadStarted()
        {
            ReloadStarted?.Invoke();
        }

        protected virtual void OnReloadStarting(CancellableEventArgs e)
        {
            ReloadStarting?.Invoke(e);
        }
    }
}
