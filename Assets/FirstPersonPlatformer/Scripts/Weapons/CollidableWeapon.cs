﻿using System;
using UnityEngine;

namespace FirstPersonPlatformer.Weapons
{
    public class CollidableWeapon : MonoBehaviour
    {
        public int Damage;

        public bool SelfDestroy = true;
        public float DestructionDelay = 10f;

        public string HitAnimationTrigger = "Hit";

        void OnCollisionEnter(Collision collision)
        {
            DoDamage(collision.gameObject);
        }

        private void DoDamage(GameObject target)
        {
            Debug.Log($"{name} collided with " + target.name);

            if (!String.IsNullOrEmpty(HitAnimationTrigger))
            {
                GetComponent<Animator>().SetTrigger(HitAnimationTrigger);
            }

            target.GetComponent<IDamageable>()?.TakeDamage(Damage);

            if (SelfDestroy)
            {
                Destroy(gameObject, DestructionDelay);
            }
        }
    }
}
