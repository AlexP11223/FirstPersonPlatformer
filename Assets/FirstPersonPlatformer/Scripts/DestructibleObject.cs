﻿using System;
using UnityEngine;

namespace FirstPersonPlatformer
{
    public class DestructibleObject : MonoBehaviour, IDamageable
    {
        public float Health = 100f;

        public string AnimationTrigger = "Destroyed";

        public bool DisableCollider = false;

        public delegate void ObjectDestroyedDelegate(GameObject obj);
        public event ObjectDestroyedDelegate Destroyed;

        public delegate void ObjectDamagedDelegate(GameObject obj, float damage, float healthBefore);
        public event ObjectDamagedDelegate Damaged;

        public bool IsDead => Health <= 0;

        public void TakeDamage(float damage)
        {
            OnDamaged(gameObject, damage, Health);

            if (IsDead)
            {
                return;
            }

            Health -= damage;

            if (IsDead)
            {
                DestroySelf();
            }
        }

        private void DestroySelf()
        {
            Debug.Log($"{name} is destroyed");

            if (!String.IsNullOrEmpty(AnimationTrigger))
            {
                GetComponent<Animator>().SetTrigger(AnimationTrigger);
            }

            if (DisableCollider)
            {
                GetComponent<Collider>().enabled = false;
            }

            OnDestroyed(gameObject);
        }

        protected virtual void OnDestroyed(GameObject obj)
        {
            Destroyed?.Invoke(obj);
        }

        protected virtual void OnDamaged(GameObject obj, float damage, float health)
        {
            Damaged?.Invoke(obj, damage, health);
        }
    }
}
