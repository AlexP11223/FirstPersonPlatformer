﻿namespace FirstPersonPlatformer
{
    public interface IDamageable
    {
        void TakeDamage(float damage);
    }
}
