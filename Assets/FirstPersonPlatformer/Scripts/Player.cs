﻿using FirstPersonPlatformer.BulletTime;
using FirstPersonPlatformer.Helpers;
using FirstPersonPlatformer.Ui;
using FirstPersonPlatformer.Weapons;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace FirstPersonPlatformer
{
    [RequireComponent(typeof(BulletTimeController))]
    [RequireComponent(typeof(DestructibleObject))]
    public class Player : MonoBehaviour
    {
        public Gun CurrentGun;

        private BulletTimeController _bulletTime;

        private DestructibleObject _destructibleObject;

        private FirstPersonController _firstPersonController;

        private PlayerUi _playerUi;

        private PauseManager _pauseManager;
        private GameManager _gameManager;

        void Start()
        {
            _bulletTime = GetComponent<BulletTimeController>();

            _destructibleObject = GetComponent<DestructibleObject>();

            _firstPersonController = GetComponent<FirstPersonController>();

            _playerUi = GameObject.FindObjectOfType<PlayerUi>();

            _pauseManager = GameObject.FindObjectOfType<PauseManager>();
            _gameManager = GameObject.FindObjectOfType<GameManager>();

            _destructibleObject.Destroyed += o =>
            {
                _bulletTime.Enable();
                _firstPersonController.MouseLook.MinimumX = -40;
                _firstPersonController.MouseLook.MaximumX = 40;
                _firstPersonController.WalkSpeed = 2;
                _firstPersonController.RunSpeed = 3.5f;
                _firstPersonController.GravityMultiplier = 3f;

                if (CurrentGun != null)
                {
                    if (CurrentGun is AssaultRifle)
                    {
                        ((AssaultRifle) CurrentGun).Spread *= 6;
                    }

                    CurrentGun.ReloadStarting += e =>
                    {
                        e.Cancelled = true;
                    };
                }

                this.ExecuteAfterDelay(1f, () =>
                {
                    _firstPersonController.enabled = false;
                });

                this.ExecuteAfterDelay(2f, () =>
                {
                    _gameManager.GameOver();
                    enabled = false;
                });
            };

            _destructibleObject.Damaged += (o, damage, healthBefore) =>
            {
                if (damage > 1)
                {
                    float health = Mathf.Clamp(healthBefore - damage, 0f, 100f);
                    _playerUi.TakeHit(fadeTime: _destructibleObject.IsDead ? 99f : (100f - health) / 3f);
                }
            };
        }

        void Update()
        {
            if (_pauseManager.IsPaused)
            {
                return;
            }

            if (CurrentGun != null)
            {
                CurrentGun.HandleInput();
            }

            _bulletTime.HandleInput();

            UpdateUi();
        }

        void UpdateUi()
        {
            _playerUi.UpdateGun(CurrentGun);
            _playerUi.UpdateHealth(_destructibleObject.Health);
            _playerUi.UpdateBulletTime(_bulletTime);
        }
    }
}
