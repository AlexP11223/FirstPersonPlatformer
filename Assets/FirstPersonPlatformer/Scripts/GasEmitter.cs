﻿using UnityEngine;

namespace FirstPersonPlatformer
{
    public class GasEmitter : MonoBehaviour
    {
        private ParticleSystem _gasParticicles;
        
        void Start()
        {
            _gasParticicles = GetComponentInChildren<ParticleSystem>();
        }

        public void Enable()
        {
            _gasParticicles.Play(true);
        }

        public void Disable()
        {
            _gasParticicles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }
    }
}
