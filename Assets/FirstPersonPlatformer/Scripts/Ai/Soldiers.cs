﻿using System.Collections.Generic;
using FirstPersonPlatformer.Helpers;
using UnityEngine;

namespace FirstPersonPlatformer.Ai
{
    public static class Soldiers
    {
        public static void SendToAttack(GameObject soldiersGroup, Transform target)
        {
            var soldiers = soldiersGroup.GetComponentsInChildren<Soldier>();
            foreach (var soldier in soldiers)
            {
                soldier.Attack(target);
            }
        }

        public static void AttackStationary(GameObject soldiersGroup, Transform target, float range)
        {
            int i = 0;
            var soldiers = soldiersGroup.GetComponentsInChildren<Soldier>();
            foreach (var soldier in soldiers)
            {
                soldier.ExecuteAfterDelay(i++ * 0.1f, () =>
                {
                    soldier.SetRange(range);
                    soldier.AttackStationary(target);
                });
            }
        }

        public static Soldier[] FindAll(Transform group)
        {
            var result = new List<Soldier>();
            FindAllInternal(group, result);
            return result.ToArray();
        }

        private static void FindAllInternal(Transform group, List<Soldier> result)
        {
            foreach (Transform child in group)
            {
                var soldier = child.GetComponent<Soldier>();
                if (soldier != null)
                {
                    result.Add(soldier);
                    continue;
                }

                FindAllInternal(child, result);
            }
        }
    }
}
