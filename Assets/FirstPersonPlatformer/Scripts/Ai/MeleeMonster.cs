﻿using System;
using FirstPersonPlatformer.Helpers;
using UnityEngine;
using UnityEngine.AI;

namespace FirstPersonPlatformer.Ai
{
    public class MeleeMonster : MonoBehaviour
    {
        private SoldierState _state = SoldierState.Idle;
        public SoldierState State
        {
            get { return _state; }
            private set
            {
                if (_state == SoldierState.Dead)
                {
                    throw new Exception($"Dead {name} cannot change state to {value}");
                }
                _state = value;
            }
        }

        private NavMeshAgent _agent;
        private NavMeshObstacle _obstacle;
        private ThirdPersonCharacter _character;
        private Animator _animator;
        private DestructibleObject _destructibleObject;

        private Transform _target;
        private DestructibleObject _targetDestructibleObject;
        private bool _isShootingDeadTarget = false;

        void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
            _obstacle = GetComponent<NavMeshObstacle>();
            _character = GetComponent<ThirdPersonCharacter>();
            _animator = GetComponent<Animator>();

            _destructibleObject = GetComponent<DestructibleObject>();
                
            _destructibleObject.Damaged += (o, damage, health) =>
            {
                //_animator.Damage();
            };

            _destructibleObject.Destroyed += o =>
            {
                Die();
            };
        }

        void Update()
        {
            if (_target == null)
            {
                if (!_obstacle.enabled)
                {
                    Stop();
                }
                return;
            }

            switch (State)
            {
                case SoldierState.Moving:
                case SoldierState.Following:
                    MoveToTarget();
                    break;
                case SoldierState.Attacking:
                    Attack();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void AttackEvent()
        {
            _targetDestructibleObject.TakeDamage(10f);
        }

        public void Move(Transform target)
        {
            State = SoldierState.Moving;
            _target = target;
        }

        public void Follow(Transform target)
        {
            State = SoldierState.Following;
            _target = target;
        }

        public void Attack(Transform target)
        {
            if (State == SoldierState.Dead)
            {
                return;
            }

            State = SoldierState.Attacking;
            _target = target;
            _targetDestructibleObject = _target.GetComponent<DestructibleObject>();
            _isShootingDeadTarget = false;
        }

        public void AttackStationary(Transform target)
        {
            if (State == SoldierState.Dead)
            {
                return;
            }

            Attack(target);
            State = SoldierState.AttackingStationary;
        }

        public void Stop()
        {
            if (State == SoldierState.Dead) // already stoped (e.g. killed target but got killed next second)
            {
                return;
            }

            if (_agent.enabled && !_agent.isStopped)
            {
                _agent.isStopped = true;
                _agent.velocity = Vector3.zero;
            }

            _agent.enabled = false;
            _obstacle.enabled = true;

            _target = null;
            _targetDestructibleObject = null;
            _isShootingDeadTarget = false;

            State = SoldierState.Idle;
        }

        private void Die()
        {
            Stop();
            State = SoldierState.Dead;
        }

        private void MoveToTarget()
        {
            _obstacle.enabled = false;
            _agent.enabled = true;

            _agent.SetDestination(_target.position);
            _agent.isStopped = false;

            if (Mathf.Approximately(_agent.remainingDistance, 0f) && _agent.pathPending)
            {
                return;
            }

            if (_agent.remainingDistance > _agent.stoppingDistance)
            {
                _character.Move(_agent.desiredVelocity, false, false);
            }
            else
            {
                _character.Move(Vector3.zero, false, false);

                if (State == SoldierState.Moving)
                {
                    Stop();
                }
            }
        }

        private void Attack()
        {
            if (_targetDestructibleObject != null && _targetDestructibleObject.IsDead)
            {
                if (!_isShootingDeadTarget)
                {
                    _isShootingDeadTarget = true;
                    this.ExecuteAfterDelay(UnityEngine.Random.Range(1f, 6f), Stop);
                }
            }

            if (CanHitTarget())
            {
                if (_agent.enabled && !_agent.isStopped)
                {
                    _character.Move(Vector3.zero, false, false);
                    _agent.isStopped = true;
                    _agent.velocity = Vector3.zero;
                    return;
                }

                if (!_agent.enabled || _agent.isStopped)
                {
                    _animator.SetTrigger("Attack"); 

                    _character.RotateTo(_target); 

                    return;
                }
            }
            else
            {
                MoveToTarget();
            }
        } 

        private bool CanHitTarget()
        {
            return Vector3.Distance(transform.position, _target.position) < 5f;
        }
    }
}