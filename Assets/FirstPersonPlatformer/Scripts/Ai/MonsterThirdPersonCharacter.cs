using UnityEngine;
using UnityEngine.Assertions;

namespace FirstPersonPlatformer.Ai
{
    public class MonsterThirdPersonCharacter : ThirdPersonCharacter
    { 
        new void Start()
        {
            base.Start(); 
        }

        protected override void UpdateAnimator(Vector3 move)
        {
            if (IsMoving)
            {

                 
                Animator.SetFloat("Speed", m_ForwardAmount, 0.1f, Time.deltaTime);
            }
            else
            {
                Animator.SetFloat("Speed", 0f);
            }
        }
    }
}