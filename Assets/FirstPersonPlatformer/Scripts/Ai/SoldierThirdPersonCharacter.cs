using UnityEngine;
using UnityEngine.Assertions;

namespace FirstPersonPlatformer.Ai
{
    [RequireComponent(typeof(SoldierActions))]
    public class SoldierThirdPersonCharacter : ThirdPersonCharacter
    {
        private SoldierActions _actions;

        new void Start()
        {
            base.Start();

            _actions = GetComponent<SoldierActions>();
        }


        protected override void UpdateAnimator(Vector3 move)
        {
            if (IsMoving)
            {
                Assert.IsTrue(IsGrounded, $"isGrounded, {name}"); // cannot jump

                _actions.Run();
                Animator.SetFloat("Speed", m_ForwardAmount, 0.1f, Time.deltaTime);
            }
            else
            {
                _actions.Stay();
            }
        }
    }
}