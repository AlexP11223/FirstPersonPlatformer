﻿using System;
using FirstPersonPlatformer.Helpers;
using FirstPersonPlatformer.Weapons;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

namespace FirstPersonPlatformer.Ai
{
    public enum SoldierState
    {
        Idle,
        Moving,
        Following,
        Attacking,
        AttackingStationary,
        Dead
    }

    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(NavMeshObstacle))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    [RequireComponent(typeof(SoldierActions))]
    [RequireComponent(typeof(DestructibleObject))]
    public class Soldier : MonoBehaviour
    {
        public float PreferredMinShootingDistance = 50f;

        private SoldierState _state = SoldierState.Idle;
        public SoldierState State
        {
            get { return _state; }
            private set
            {
                if (_state == SoldierState.Dead)
                {
                    throw new Exception($"Dead {name} cannot change state to {value}");
                }
                _state = value;
            }
        }

        private NavMeshAgent _agent;
        private NavMeshObstacle _obstacle;
        private ThirdPersonCharacter _character;
        private SoldierActions _actions;
        private DestructibleObject _destructibleObject;

        private AssaultRifle _gun;
        private Transform _shotCheckStartPosition;
        private int _shotCheckLayerMask = -5; // -5 - default Ignore Raycast

        private Transform _target;
        private DestructibleObject _targetDestructibleObject;
        private bool _isShootingDeadTarget = false;

        void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
            _obstacle = GetComponent<NavMeshObstacle>();
            _character = GetComponent<ThirdPersonCharacter>();
            _actions = GetComponent<SoldierActions>();

            _shotCheckStartPosition = gameObject.FindComponentInChildWithTag<Transform>(Tags.ShotCheckStartPosition);

            int ownerLayer = gameObject.layer;
            Assert.AreNotEqual(ownerLayer, Layers.Default, $"{name} must not be in Default layer");
            _shotCheckLayerMask &= ~(1 << ownerLayer);
            _shotCheckLayerMask &= ~(1 << Layers.Player);

            _gun = gameObject.FindComponentInChildWithTag<AssaultRifle>(Tags.Weapon);
            if (_gun != null)
            {
                _gun.ReloadStarted += () =>
                {
                    _actions.Reload();
                };
            }

            _destructibleObject = GetComponent<DestructibleObject>();
                
            _destructibleObject.Damaged += (o, damage, health) =>
            {
                _actions.Damage();
            };

            _destructibleObject.Destroyed += o =>
            {
                Die();
            };
        }

        void Update()
        {
            if (_target == null)
            {
                if (!_obstacle.enabled)
                {
                    Stop();
                }
                return;
            }

            switch (State)
            {
                case SoldierState.Moving:
                case SoldierState.Following:
                    MoveToTarget();
                    break;
                case SoldierState.Attacking:
                case SoldierState.AttackingStationary:
                    Attack();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void ShootEvent()
        {
            _gun.Shoot(_shotCheckStartPosition);
        }

        public void Move(Transform target)
        {
            State = SoldierState.Moving;
            _target = target;
        }

        public void Follow(Transform target)
        {
            State = SoldierState.Following;
            _target = target;
        }

        public void Attack(Transform target)
        {
            if (State == SoldierState.Dead)
            {
                return;
            }

            State = SoldierState.Attacking;
            _target = target;
            _targetDestructibleObject = _target.GetComponent<DestructibleObject>();
            _isShootingDeadTarget = false;
        }

        public void AttackStationary(Transform target)
        {
            if (State == SoldierState.Dead)
            {
                return;
            }

            Attack(target);
            State = SoldierState.AttackingStationary;
        }

        public void Stop()
        {
            if (State == SoldierState.Dead) // already stoped (e.g. killed target but got killed next second)
            {
                return;
            }

            if (_agent.enabled && !_agent.isStopped)
            {
                _agent.isStopped = true;
                _agent.velocity = Vector3.zero;
            }

            _agent.enabled = false;
            _obstacle.enabled = true;

            _target = null;
            _targetDestructibleObject = null;
            _isShootingDeadTarget = false;

            State = SoldierState.Idle;
        }

        public void SetRange(float preferredMinDist, float gunRange = -1)
        {
            Assert.IsNotNull(_gun);

            PreferredMinShootingDistance = preferredMinDist;
            _gun.Range = gunRange > 0f ? gunRange : (preferredMinDist + 30f);
        }

        private void Die()
        {
            Stop();
            State = SoldierState.Dead;
            _actions.Die();
        }

        private void MoveToTarget()
        {
            _obstacle.enabled = false;
            _agent.enabled = true;

            _agent.SetDestination(_target.position);
            _agent.isStopped = false;

            if (Mathf.Approximately(_agent.remainingDistance, 0f) && _agent.pathPending)
            {
                return;
            }

            if (_agent.remainingDistance > _agent.stoppingDistance)
            {
                _character.Move(_agent.desiredVelocity, false, false);
            }
            else
            {
                _character.Move(Vector3.zero, false, false);

                if (State == SoldierState.Moving)
                {
                    Stop();
                }
            }
        }

        private void Attack()
        {
            if (_targetDestructibleObject != null && _targetDestructibleObject.IsDead)
            {
                if (!_isShootingDeadTarget)
                {
                    _isShootingDeadTarget = true;
                    this.ExecuteAfterDelay(UnityEngine.Random.Range(1f, 6f), Stop);
                }

                if (_gun.IsReloading)
                {
                    Stop();
                    return;
                }
            }

            if (CanShootTarget())
            {
                if (_agent.enabled && !_agent.isStopped)
                {
                    if (Vector3.Distance(transform.position, _target.position) < PreferredMinShootingDistance)
                    {
                        _character.Move(Vector3.zero, false, false);
                        _agent.isStopped = true;
                        _agent.velocity = Vector3.zero;
                        return;
                    }
                }

                if (!_agent.enabled || _agent.isStopped)
                {
                    if (!_gun.IsReloading)
                    {
                        _actions.Attack();
                    }

                    _character.RotateTo(_target);

                    RotateAimTo(_shotCheckStartPosition, _target);

                    return;
                }
            }

            if (State != SoldierState.AttackingStationary)
            {
                MoveToTarget();
            }
        }

        public void RotateAimTo(Transform obj, Transform target)
        { 
            Vector3 direction = target.position - obj.position;

            Quaternion rot = Quaternion.LookRotation(direction);

            // slerp to the desired rotation over time
            obj.rotation = Quaternion.Slerp(obj.rotation, rot, 0.5f * Time.deltaTime); 
        }

        private bool CanShootTarget()
        {
            if (Vector3.Distance(transform.position, _target.position) < _gun.Range)
            {
                Debug.DrawLine(_shotCheckStartPosition.position, _target.position, Color.red);

                if (!Physics.Linecast(_shotCheckStartPosition.position, _target.position, _shotCheckLayerMask))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
