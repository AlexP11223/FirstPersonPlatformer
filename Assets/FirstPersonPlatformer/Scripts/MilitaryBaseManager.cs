﻿using FirstPersonPlatformer.Helpers;
using UnityEngine;

namespace FirstPersonPlatformer
{
    public class MilitaryBaseManager : MonoBehaviour
    {
        public Alarm[] Alarms { get; private set; }

        void Start()
        {
            Alarms = transform.FindComponenstInChildrenWithTag<Alarm>(Tags.AlarmLight);
        }

        public void TriggerAlarm()
        {
            Debug.Log("Alarm triggered");

            foreach (var alarm in Alarms)
            {
                alarm.Trigger();
            }
        }

        public void StopAlarm()
        {
            Debug.Log("Alarm stopped");

            foreach (var alarm in Alarms)
            {
                alarm.Stop();
            }
        }
    }
}
