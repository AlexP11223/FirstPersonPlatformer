﻿using UnityEngine;

namespace FirstPersonPlatformer.BulletTime
{
    public class TimeManager : MonoBehaviour
    {
        public delegate void TimeSpeedChangedDelegate(float timeSpeed);
        public static event TimeSpeedChangedDelegate TimeSpeedChanged;

        public static float CurrentTimeSpeed => Time.timeScale;

        public static bool IsNormalTimeSpeed => Mathf.Approximately(CurrentTimeSpeed, 1.0f);

        public static bool IsTimeStopped => Mathf.Approximately(CurrentTimeSpeed, 0.0f);

        public static void ChangeTimeSpeed(float timeSpeed)
        {
            Time.timeScale = timeSpeed;
            Time.fixedDeltaTime = Time.timeScale * .02f;

            OnTimeSpeedChanged(timeSpeed);
        }

        public static void EnableSlowMotion(float timeSpeed)
        {
            ChangeTimeSpeed(timeSpeed);
        }

        public static void StopSlowMotion()
        {
            ChangeTimeSpeed(1.0f);
        }

        private static void OnTimeSpeedChanged(float timeSpeed)
        {
            TimeSpeedChanged?.Invoke(timeSpeed);
        }
    }
}
