﻿using UnityEngine;

namespace FirstPersonPlatformer.BulletTime
{
    public class BulletTimeController : MonoBehaviour
    {
        public float Speed = 0.4f;

        [Range(0f, 1.0f)]
        public float Charge = 1.0f;

        [Range(0f, 0.5f)]
        public float Cost = 0.1f;

        [Range(0f, 0.1f)]
        public float ReloadSpeed = 0.05f;
        
        public bool IsActive => !TimeManager.IsNormalTimeSpeed && !TimeManager.IsTimeStopped;

        public void HandleInput()
        {
            if (Input.GetButtonDown("BulletTime"))
            {
                Toggle();
            }
        }

        public void Toggle()
        {
            if (IsActive)
            {
                TimeManager.StopSlowMotion();
            }
            else
            {
                TimeManager.ChangeTimeSpeed(Speed);
            }
        }

        public void Enable()
        {
            if (!IsActive)
            {
                Toggle();
            }
        }

        void Update() 
        {
            if (IsActive)
            {
                Charge = Mathf.Clamp01(Charge - Cost * Time.unscaledDeltaTime);
                if (Mathf.Approximately(Charge, 0f))
                {
                    Toggle();
                }
            }
            else
            {
                Charge = Mathf.Clamp01(Charge + ReloadSpeed * Time.deltaTime);
            }
        }
    }
}
