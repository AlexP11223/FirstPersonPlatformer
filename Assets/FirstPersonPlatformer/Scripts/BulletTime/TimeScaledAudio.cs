﻿using System;
using UnityEngine;

namespace FirstPersonPlatformer.BulletTime
{
    [RequireComponent(typeof(AudioSource))]
    public class TimeScaledAudio : MonoBehaviour
    {
        private AudioSource _audio;

        void Start()
        {
            _audio = GetComponent<AudioSource>();

            _audio.pitch = TimeManager.CurrentTimeSpeed;
            TimeManager.TimeSpeedChanged += OnTimeSpeedChanged;
        }

        void OnDestroy()
        {
            TimeManager.TimeSpeedChanged -= OnTimeSpeedChanged;
        }

        private void OnTimeSpeedChanged(float timeSpeed)
        {
            _audio.pitch = timeSpeed;
        }
    }
}
