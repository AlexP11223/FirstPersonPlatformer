﻿namespace FirstPersonPlatformer
{
    public static class Tags
    {
        public const string Player = "Player";
        public const string AlarmLight = "AlarmLight";
        public const string ShotCheckStartPosition = "ShotCheckStartPosition";
        public const string Weapon = "Weapon";
        public const string Blood = "Blood";
        public const string Terrain = "Terrain";
        public const string Parachute = "Parachute";
        public const string Head = "Head";
    }

    public static class Layers
    {
        public const int Default = 0;
        public const int Player = 8;
        public const int Gun = 9;
    }

    public static class Scenes
    {
        public const string MainMenu = "MainMenu";
        public const string Level1 = "Level1";
        public const string Level2 = "Level2";
    }
}
