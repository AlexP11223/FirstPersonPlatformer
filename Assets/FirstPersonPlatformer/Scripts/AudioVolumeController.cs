﻿using System.Linq;
using UnityEngine;

namespace FirstPersonPlatformer
{
    public class AudioVolumeController : MonoBehaviour
    {
        public AudioSource[] AudioSources;

        public void SetVolume(float volume)
        {
            foreach (var audioSource in AudioSources.Where(a => a != null))
            {
                audioSource.volume = volume;
            }
        }
    }
}
