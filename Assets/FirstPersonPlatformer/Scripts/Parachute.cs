﻿using FirstPersonPlatformer.Helpers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityStandardAssets.Characters.FirstPerson;

namespace FirstPersonPlatformer
{
    [RequireComponent(typeof(Animator))]
    public class Parachute : MonoBehaviour
    {
        public MouseLook ParachuteMouseLook;

        public float ParachuteWalkSpeed = 2f;
        public float ParachuteRunSpeed = 3f;

        public bool DestroyOnLand = true;

        public bool IsOpened { get; private set; }

        private Animator _animator;

        private FirstPersonController _fpController;

        private readonly MouseLook _defaultMouseLook = new MouseLook();
        private float _defaultWalkSpeed = 2f;
        private float _defaultRunSpeed = 3f;

        void Start()
        {
            _animator = GetComponent<Animator>();
            _fpController = GetComponent<FirstPersonController>();
        }

        public void Open()
        {
            Assert.IsFalse(IsOpened);
            Assert.IsTrue(isActiveAndEnabled);

            Debug.Log("Opened parachute");

            _animator.SetBool("Parachuting", true);

            if (_fpController != null)
            {
                Assign(_fpController.MouseLook, _defaultMouseLook);

                Assign(ParachuteMouseLook, _fpController.MouseLook);

                _defaultWalkSpeed = _fpController.WalkSpeed;
                _defaultRunSpeed = _fpController.RunSpeed;

                _fpController.WalkSpeed = ParachuteWalkSpeed;
                _fpController.RunSpeed = ParachuteRunSpeed;
            }

            IsOpened = true;
        }

        public void Land()
        {
            Assert.IsTrue(IsOpened);

            Debug.Log("Landed with parachute");

            _animator.SetBool("Parachuting", false);

            if (_fpController != null)
            {
                this.ExecuteAfterDelay(0.5f, () =>
                {
                    Assign(_defaultMouseLook, _fpController.MouseLook);

                    _fpController.WalkSpeed = _defaultWalkSpeed;
                    _fpController.RunSpeed = _defaultRunSpeed;
                });
            }

            IsOpened = false;

            if (DestroyOnLand)
            {
                enabled = false;
                Destroy(gameObject.FindComponentInChildWithTag<Transform>(Tags.Parachute).gameObject);
            }
        }

        void OnTriggerEnter(Collider collision)
        {
            if (collision.CompareTag(Tags.Terrain) && IsOpened)
            {
                Land();
            }
        }

        private static void Assign(MouseLook src, MouseLook dest)
        {
            dest.MinimumX = src.MinimumX;
            dest.MaximumX = src.MaximumX;
            dest.XSensitivity = src.XSensitivity;
            dest.YSensitivity = src.YSensitivity;
        }
    }
}
