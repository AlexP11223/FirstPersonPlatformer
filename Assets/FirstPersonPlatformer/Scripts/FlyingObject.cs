﻿using UnityEngine;

namespace FirstPersonPlatformer
{
    public class FlyingObject : MonoBehaviour
    {
        public float Speed = 5f;

        public int DestructionTimeout = -1;

        private Vector3 _direction;

        private bool _isStopped = true;

        private float _timeElapsed;

        public void Move(Transform target)
        {
            _direction = (target.position - transform.position).normalized;
            transform.LookAt(target);
            _isStopped = false;
            _timeElapsed = 0;
        }

        void OnCollisionEnter(Collision collision)
        {
            _isStopped = true;
        }

        void Update()
        {
            if (!_isStopped)
            {
                _timeElapsed += Time.deltaTime;
                if (DestructionTimeout >= 0 && _timeElapsed > DestructionTimeout)
                {
                    Debug.Log($"Destroyed infinitely flying {gameObject.name}");
                    Destroy(gameObject);
                    return;
                }

                transform.position += _direction * Speed * Time.deltaTime;
            }
        }
    }
}
