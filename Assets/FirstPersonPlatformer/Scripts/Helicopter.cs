﻿using UnityEngine;

namespace FirstPersonPlatformer
{
    public enum HelicopterGunId
    {
        Left = 0,
        Right = 1
    }

    public class Helicopter : MonoBehaviour
    {
        public GameObject[] MissileSpawns;
        public GameObject MissilePrefab;
        public AudioSource MissileLaunchSound;

        private Animator _animator;

        void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void Shoot(Transform target, HelicopterGunId gunIdId)
        {
            var missileSpawn = MissileSpawns[(int) gunIdId];

            var missile = Instantiate(MissilePrefab, missileSpawn.transform);
            missile.transform.localPosition = Vector3.zero;
            missile.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
            missile.transform.parent = null;

            missile.GetComponent<FlyingObject>().Move(target);

            MissileLaunchSound.Play();
        }

        public void Move(Transform target)
        {
            _animator.SetBool("Moving", true);

            GetComponent<FlyingObject>().Move(target);
        }

        public void RotateLeft()
        {
            _animator.SetTrigger("RotateLeft");
        }
    }
}
