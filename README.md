A simple First-Person Shooter/Action with 2 levels. Unity 2017.4.

On the first level the player lands on an island and attacks enemy base (with some helicopter support - until it flies away evading defense systems).

When player enters the main building, he gets into a trap — a locked room filling with poisonous gas. He has to destroy the door by using explosive barrels or just shooting into it.

The last room is a science laboratory. When player enters, the guards kill scientists to prevent their capture and information leak, and shortly get killed by the player. But then something goes wrong with the equipment and player gets sucked into the portal to another world.

The second level is currently very incomplete, because I decided to focus more on the first level after failing to find some assets, but the idea is that the player fights monsters and overcomes challenges to return back to our world.

## Some features:

- DIfferent hit effects/bullet holes depending on objects.
- Some objects (alarms, lights, explosive barrels,  ...) can be destroyed.
- Some objects (pallets, barrels, ...) can be pushed.
- Headshots deal more damage.
- Bullet spread (higher for enemies, low for player).
- Random blood splashes on screen when taking damage.
- "Bullet time"/slow motion, that can be activated manually or in some cases automatically (such as when entering a room with enemies aiming at the door).
- Slow motion affects most audio sources and hit speed.
- Sounds outside get quiter when entering building.
- Health regeneration after not receiving damage for some time.
- Checkpoints/loading.

## Not implemented features (because of lack of time or assets)

- Main character model. Didn't find a suitable free model with at least some animations.
- Bullet time is not really **bullet** because there is no bullets (just raycasts).
- Different death animations (there are several death animations provided with soldier model, but they need to be adjusted for weapon).
- Multiple weapons: the player was supposed to take laser gun from the table in the lab.
- Footstep sound depending on surface. Difficult to find good free sounds with consistent style.
- Weapon recoil animation.


![](https://drive.google.com/uc?export=download&id=10k1DyTsSlugAIymRdurdIixWhnHPLk1w&.mp4)

![](https://i.imgur.com/yaJ0H2F.png)

(freezes in some videos are caused by recording, the game itself runs fine on my PC)

![](https://drive.google.com/uc?export=download&id=14NzMPrjzpISV24-wD_PaqRVnqiGyAkQa&.mp4) ![](https://drive.google.com/uc?export=download&id=1FAvrc2VdIR_iG9AyoOV6Yfw-bVGBgE7m&.mp4) ![](https://drive.google.com/uc?export=download&id=1_XUlaUXBF_8UyuN6RI17CspjPXLVJqYC&.mp4)

![](https://drive.google.com/uc?export=download&id=14C2Iq0YpI_5jEIHM0PY_KDTOShPn4BOk&.mp4)

![](https://i.imgur.com/AC2fjaG.png)

![](https://i.imgur.com/474hFCy.png)

![](https://drive.google.com/uc?export=download&id=1gYq9rqYyNiLEbjwhQQzF7oHQfOvdVscq&.mp4)

![](https://i.imgur.com/LKyrs8A.png)

![](https://i.gyazo.com/189c46683083de21f4bc61aaedd220fc.mp4)

![](https://i.imgur.com/EvfDkz9.png?1)

# Required assets

Some of the assets are not included in the repository because their license does not allow that (even though they are free). Import them from the Unity Asset Store.

- [Nature Starter Kit 2](https://assetstore.unity.com/packages/3d/environments/nature-starter-kit-2-52977) (only bushes from Nature folder) 
- [War FX](https://assetstore.unity.com/packages/vfx/particles/war-fx-5669) 

# Attribution

## Audio

[Body falling to floor 7](https://freesound.org/people/JakLocke/sounds/261296/) by [JakLocke](https://freesound.org/people/JakLocke/) under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) license.

[Door Smash 1](http://freesound.org/people/qubodup/sounds/218890) by [Iwan ‘qubodup’ Gabovitch](http://freesound.org/people/qubodup) under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) license.

[Parachute opening sound](https://freesound.org/people/Huggy13ear/sounds/138960/) by Huggy13ear under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) license.

[Proximity Card Door Lock](https://freesound.org/people/StrangeAcoustics/sounds/346049/) by StrangeAcoustics under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) license.

[Wind Howling NIghttime](https://freesound.org/people/Dynamicell/sounds/17553/) by [Dynamicell](https://freesound.org/people/Dynamicell/) under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) license.



## Attack Helicopter, Missile Launcher

[Attack Helicopter II](https://assetstore.unity.com/packages/3d/vehicles/air/attack-helicopter-ii-animations-8405) and [Missile Launcher](https://assetstore.unity.com/packages/3d/props/weapons/missile-launcher-3759) assets by Duane's Mind.

## Barrels

[PBR Barrels](https://assetstore.unity.com/packages/3d/props/industrial/pbr-barrels-83508) asset by S1mpleQ.

## Blood splatter

[Blood splatter decal package](https://assetstore.unity.com/packages/2d/textures-materials/blood-splatter-decal-package-7518) asset by NorSat Entertainment.

## Bulk Container

Pallets and container from [Bulk Container](https://assetstore.unity.com/packages/3d/props/industrial/bulk-container-110387) asset by ESsplashkid.

## Building Kit 1

Doors and windows from [Building Kit 1](https://assetstore.unity.com/packages/3d/environments/industrial/building-kit-1-68179) asset by Mojo-Structure.

## Island Collection

Terrain textures and skybox from [Free Island Collection](https://assetstore.unity.com/packages/3d/environments/landscapes/free-island-collection-104753) asset by Bicameral Studios.

## JSON .NET for Unity

[JSON .NET for Unity](https://assetstore.unity.com/packages/tools/input-management/json-net-for-unity-11347) asset by parentElement.

## Lamps

[Free PBR Lamps](https://assetstore.unity.com/packages/3d/props/interior/free-pbr-lamps-70181) asset by New Solution Studio.

## Man in a Suit

[Man in a Suit](https://assetstore.unity.com/packages/3d/characters/humanoids/man-in-a-suit-51662) asset by Studio New Punch.

## Missile

[Free Missile](https://assetstore.unity.com/packages/3d/props/weapons/free-missile-72692) asset by Sabri Ayes.

## Modern Russian Assault Rifle

[Modern Russian Assault Rifle](https://assetstore.unity.com/packages/3d/props/guns/modern-russian-assault-rifle-46033) asset by Perlind Arts.

## Plank Textures PBR

[Plank Textures PBR](https://assetstore.unity.com/packages/2d/textures-materials/wood/plank-textures-pbr-72318) asset by A Dog's Life Software.

## Roof textures

[Roof textures](https://assetstore.unity.com/packages/2d/textures-materials/roof-textures-5844) asset by Lăng Vũ Văn.

## Sci-Fi Styled Modular Pack

Doors, floor, windows from [Sci-Fi Styled Modular Pack](https://assetstore.unity.com/packages/3d/environments/sci-fi/sci-fi-styled-modular-pack-82913) asset by karboosx.

## Sci-Fi Texture Pack

[Sci-Fi Texture Pack 1](https://assetstore.unity.com/packages/2d/textures-materials/sci-fi-texture-pack-1-23301) and [Sci-Fi Texture Pack 2](https://assetstore.unity.com/packages/2d/textures-materials/sci-fi-texture-pack-2-42026) assets by FireBolt Studios.

## Sci-Fi Weapons

[Sci-Fi Weapons](http://devassets.com/assets/sci-fi-weapons/) asset by Taylor Huff.

## Shipping Containers

Containers and walkways from [Free Shipping Containers](https://assetstore.unity.com/packages/3d/environments/industrial/free-shipping-containers-18315) asset by Calvin Weibel.

## Yughues Free Concrete Materials

[Yughues Free Concrete Materials](https://assetstore.unity.com/packages/2d/textures-materials/concrete/yughues-free-concrete-materials-12951) asset by Nobiax / Yughues under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.
